package blink.tor;

import android.app.*;
import android.content.*;
import android.os.*;

public class Logo extends Activity{
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		setContentView(R.layout.logo);
		new Handler().postDelayed(new Runnable(){
			public void run(){
				finish();
				startActivity(new Intent(Logo.this,MainActivity.class)/*.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)*/);
				overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
			}
		},1500);
	}
}
