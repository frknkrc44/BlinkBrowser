package blink.tor;

import android.app.*;
import android.os.*;
import android.content.*;
import android.widget.*;
import android.widget.AdapterView.*;
import android.view.*;

public class Settings extends Activity{
	String[] onion = {"onion","onion.link","onion.cab",
						"tor2web.org","onion.sh","tor2web.fi",
						"onion.direct","onion.ru"};
	String[] uAgent = {"Default","Android","Desktop",
						"Mobile (Random)","Desktop (Random)",
						"Bot (Random)","Fun (Random)","User"};
	String os = "onionSelection";
	String pm = "privateMode";
	String bp = "backPlay";
	String fs = "fullScreen";
	String dC = "disableCache";
	String uA = "userAgent";
	String ca = "customAgent";
	String nb = "nBrowser";
	ArrayAdapter<String> itemm;
	Switch bl;
	Switch fb;
	Switch dc;
	Switch nt;
	int t1;
	int t2;
	String t3;
	boolean setChanged = false;
	SharedPreferences sp;
	SharedPreferences.Editor se;
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		sp = getSharedPreferences(getPackageName()+".settings",Context.MODE_PRIVATE);
		se = sp.edit();
		t1 = sp.getInt(os,1);
		t2 = sp.getInt(uA,0);
		t3 = sp.getString(ca,"");
		setContentView(R.layout.set);
		bl = (Switch) findViewById(R.id.bPlay);
		fb = (Switch) findViewById(R.id.fScr);
		dc = (Switch) findViewById(R.id.disC);
		nt = (Switch) findViewById(R.id.nTar);
		getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.abar));
		getActionBar().setDisplayShowCustomEnabled(true);
		getActionBar().setIcon(android.R.color.transparent);
		bl.setChecked(sp.getBoolean(bp,false));
		fb.setChecked(sp.getBoolean(fs,false));
		dc.setChecked(sp.getBoolean(dC,false));
		nt.setChecked(sp.getBoolean(nb,false));
		
		bl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton p1, boolean p2){
				se.putBoolean(bp,p2);
				setChanged = true;
			}
		});
		
		fb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton p1, boolean p2){
				se.putBoolean(fs,p2);
				setChanged = true;
			}
		});
		
		dc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton p1, boolean p2){
				se.putBoolean(dC,p2);
				setChanged = true;
			}
		});
		
		nt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton p1, boolean p2){
				se.putBoolean(nb,p2);
				setChanged = true;
			}
		});
		
		itemm = new ArrayAdapter<String>
		(this, android.R.layout.simple_list_item_1, android.R.id.text1);
		ListView lv = (ListView) findViewById(R.id.list);
		itemm.add(getResources().getString(R.string.oprv)+": "+onion[sp.getInt(os,1)]);
		itemm.add(getResources().getString(R.string.kuar)+": "+uAgent[sp.getInt(uA,0)]);
		lv.setAdapter(itemm);
		lv.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> av, View v, int p, long id){
				menuAc(p == 0);
			}
		});
		
		((Button) findViewById(R.id.b1)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				se.commit();
				finish();
			}
		});

		((Button) findViewById(R.id.b2)).setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				finish();
			}
		});
		
	}
	
	AlertDialog ad;
	AlertDialog d;
	
	void menuAc(final boolean onionSelected){
		ListView lv = new ListView(this);
		lv.setLayoutParams(new ListView.LayoutParams(
							ListView.LayoutParams.MATCH_PARENT,
							ListView.LayoutParams.WRAP_CONTENT));
		final ArrayAdapter<String> item = new ArrayAdapter<String>
		(this, android.R.layout.simple_list_item_1, android.R.id.text1);
		if(onionSelected){
			for(int i=0;i!=onion.length;i++)
				item.add(onion[i]);
			lv.setAdapter(item);
			lv.setOnItemClickListener(new OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> av, View v, int p, long id){
					se.putInt(os,p);
					t1 = p;
					refresh();
					ad.dismiss();
				}
			});
			ad = new AlertDialog.Builder(this)
				.setTitle(getResources()
				.getString(R.string.oprv))
				.setView(lv).create();
		} else {
			for(int i=0;i!=uAgent.length;i++)
				item.add(uAgent[i]);
			lv.setAdapter(item);
			lv.setOnItemClickListener(new OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> av, View v, int p, long id){
					if(p == 7){
						ad.dismiss();
						LinearLayout ll = new LinearLayout(Settings.this);
						ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
						ll.setPadding(16,16,16,16);
						final EditText et = new EditText(Settings.this);
						et.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
						et.setSingleLine(true);
						et.setText(t3);
						et.setSelection(t3.length());
						ll.addView(et);
						
						d = new AlertDialog.Builder(Settings.this)
							.setTitle(getResources()
							.getString(R.string.kuar))
							.setPositiveButton(getResources().getString(R.string.kydt), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) { 
									se.putString(ca,et.getText().toString());
									t3 = et.getText().toString();
									t2 = 7;
									se.putInt(uA,t2);
									refresh();
									d.dismiss();
								}
							})
							
							.setNegativeButton(getResources().getString(R.string.iptl), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) { 
									d.dismiss();
								}
							})
							.setView(ll).create();
							d.show();
						
					} else {
						se.putInt(uA,p);
						t2 = p;
						se.remove(ca);
					}
					refresh();
					ad.dismiss();
				}
			});
			ad = new AlertDialog.Builder(this)
				.setTitle(getResources()
				.getString(R.string.kuar))
				.setView(lv).create();
		}
		ad.show();
	}
	
	void refresh(){
		itemm.clear();
		itemm.add(getResources().getString(R.string.oprv)+": "+onion[t1]);
		itemm.add(getResources().getString(R.string.kuar)+": "+uAgent[t2]);
		setChanged = true;
	}
	
	@Override
	public void onBackPressed(){
		if(setChanged){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getResources().getString(R.string.adgs))
				.setMessage(getResources().getString(R.string.akyd))
				.setPositiveButton(getResources().getString(R.string.evet), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) { 
						se.commit();
						ad.dismiss();
						finish();
					}})
				.setNeutralButton(getResources().getString(R.string.geri), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						ad.dismiss();
					}})
				.setNegativeButton(getResources().getString(R.string.hayr), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) { 
						ad.dismiss();
						finish();
					}})
				.setIcon(android.R.drawable.ic_dialog_alert);
			ad = builder.create();
			ad.show();
		} else super.onBackPressed();
	}
	
}
