package blink.tor.additions;

import android.app.*;
import android.content.*;
import android.os.*;
import blink.tor.*;

public class BlinkWP extends Activity{
	public void onCreate(Bundle b){
		super.onCreate(b);
		try{ 
			SharedPreferences.Editor se = (getSharedPreferences(getPackageName()+".settings",Context.MODE_PRIVATE)).edit();
			se.putString("url","http://blinkcursor.org/");
			se.commit();
			startActivity(new Intent(this,Logo.class));
			finish(); 
		} catch(Exception e){}
	}
}
