package blink.tor.additions;

import android.app.*;
import android.content.*;
import android.os.*;
import android.widget.*;
import blink.tor.*;

public class LinkReceiver extends Activity{
	public void onCreate(Bundle b){
		super.onCreate(b);
		try{ 
			SharedPreferences.Editor se = (getSharedPreferences(getPackageName()+".settings",Context.MODE_PRIVATE)).edit();
			se.putString("url",getIntent().getData().toString());
			se.commit();
			startActivity(new Intent(this,Logo.class));
			finish(); 
		} catch(Exception e){
			Toast.makeText(getBaseContext(),e.getMessage(),Toast.LENGTH_LONG).show();
		}
	}
}
