package blink.tor.additions;

import android.app.*;
import android.content.*;
import android.os.*;
import android.widget.*;
import blink.tor.*;

public class LinkReceiverFromShared2 extends Activity{
	public void onCreate(Bundle b){
		super.onCreate(b);
		try{ 
			metniKopyala(getIntent().getStringExtra(Intent.EXTRA_TEXT));
			finish(); 
		} catch(Exception e){
			Toast.makeText(getBaseContext(),e.getMessage(),Toast.LENGTH_LONG).show();
		}
	}
	
	void metniKopyala(String s){
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			android.text.ClipboardManager pano = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			if(s.length() > 0) pano.setText(s);
		} else {
			android.content.ClipboardManager pano = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			android.content.ClipData clip = android.content.ClipData.newPlainText(getResources().getString(R.string.btor), s);
			if(s.length() > 0) pano.setPrimaryClip(clip);
		}
		if(s.length() > 0)
			Toast.makeText(getBaseContext(),getResources().getString(R.string.mkpy),Toast.LENGTH_LONG).show();
	}
	
}
